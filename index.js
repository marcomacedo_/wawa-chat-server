const app = require("express")();
const http = require("http").createServer(app);
const io = require("socket.io")(http, {
    cors: {
        origin: "http://localhost:4200",
        credentials: true,
        methods: ["GET", "POST", "OPTIONS"],
    },
});
const events = { chat: "chat-msg" };

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/index.html");
});

io.on("connection", (socket) => {
    console.log("a user connected");
    socket.on(events.chat, (msg) => {
        console.log(events.chat, msg);
        socket.broadcast.emit(events.chat, msg);
    });
    socket.on("disconnect", () => {
        console.log("user disconnected");
    });
});

http.listen(8000, () => {
    console.log("listening on port 8000");
});
